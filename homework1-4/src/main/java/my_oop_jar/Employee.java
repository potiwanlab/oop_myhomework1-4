package my_oop_jar;

public class Employee {

    public int id;
    public String firstname;
    public String lastname;
    private int salary;
    public String type;
    public String dressCode;
    public String role;

    public Employee() {

    }

    public Employee(String firstname, String lastname, int salary) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
    }

    public Employee(int id, String firstnameInput, String lastnameInput, int salaryInput, String type) {
        this.id = id;
        this.firstname = firstnameInput;
        this.lastname = lastnameInput;
        this.salary = salaryInput;
        this.type = type;
    }

    public Employee(int id, String firstnameInput, String lastnameInput, int salaryInput, String type, String role) {
        this.id = id;
        this.firstname = firstnameInput;
        this.lastname = lastnameInput;
        this.salary = salaryInput;
        this.type = type;
        this.role = role;
    }

    public void hello() {
        System.out.println("Hello " + this.firstname);
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void gossip(Employee employee, String massege) {
        System.out.println("Hey " + employee.firstname + ", " + massege);
    }

    public void work() {

    }

}
