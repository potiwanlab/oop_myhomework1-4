package my_oop_jar;

public class Homework9_4 {

    private static Employee[] employees;

    public static void main(String[] args) {
        String[] rawData = {
                "id:1001 firstname:Luke lastname:Skywalker salary:10000 type:frontend role:Programmer",
                "id:1002 firstname:Tony lastname:Stark salary:20000 type:tshirt role:CEO",
                "id:1003 firstname:Somchai lastname:Jaidee salary:30000 type:fullstack role:Programmer",
                "id:1004 firstname:MonkeyD lastname:Luffee salary:40000 type:maid role:OfficeCleaner"
        };

        initialEmployees(rawData);
        printEmployees();

    }

    private static void initialEmployees(String[] employeesData) {
        int size = employeesData.length;
        employees = new Employee[size];

        for (int i = 0; i < size; i++) {
            employees[i] = convertDataToEmployee(employeesData[i]);
        }

    }

    private static Employee convertDataToEmployee(String rawData) {

        String[] datas = rawData.split(" ");
        String[] employeeInfos = new String[datas.length];

        for (int i = 0; i < datas.length; i++) {
            String[] info = datas[i].split(":");
            employeeInfos[i] = info[1];
        }

        return new Employee(Integer.parseInt(employeeInfos[0]), employeeInfos[1], employeeInfos[2],
                Integer.parseInt(employeeInfos[3]),
                employeeInfos[4], employeeInfos[5]);
    }

    private static void printEmployees() {
        for (Employee employee : employees) {
            System.out.printf("id: %d firstname: %s lastname: %s salary: %d type: %s role: %s \n", employee.id,
                    employee.firstname, employee.lastname, employee.getSalary(), employee.type, employee.role);
        }
    }
}
