package my_oop_jar;

public class Programmer extends Employee {

    public Programmer(int id, String firstnameInput, String lastnameInput, int salaryInput, String type) {
        super(id, firstnameInput, lastnameInput, salaryInput, type);
    }

    @Override
    public void work() {
        createWebsite();
        fixPC();
        installWindow();
    }

    public void createWebsite() {
        System.out.println("Programmer create website");
    }

    public void fixPC() {
        System.out.println("Programmer fix PC");
    }

    public void installWindow() {
        System.out.println("Programmer install window");
    }

}
