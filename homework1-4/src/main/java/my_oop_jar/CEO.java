package my_oop_jar;

public class CEO extends Employee {

    public CEO(int id, String firstnameInput, String lastnameInput, int salaryInput, String type) {
        super(id, firstnameInput, lastnameInput, salaryInput, type);
    }

    public CEO(String firstname, String lastname, int salary) {
        super(firstname, lastname, salary);
    }

    private Programmer[] employees;

    public void initialEmployees(String[] employeesData) {
        int size = employeesData.length;
        employees = new Programmer[size];

        for (int i = 0; i < size; i++) {
            employees[i] = convertDataToProgrammer(employeesData[i]);
        }

    }

    public int getSalary() {
        return super.getSalary() * 2;
    };

    public void work(Employee luckyEmployee) {
        this.fire(luckyEmployee);
        this.hire(luckyEmployee);
        this.seminar();
        this.golf();
    }

    private void seminar() {
        this.dressCode = "suit";
        System.out.println("He is going to seminar Dress with : " + this.dressCode);
    }

    private void hire(Employee luckyEmployee) {
        luckyEmployee.dressCode = "tshirt";
        System.out.println(luckyEmployee.firstname + " has been hired back! Dress with : " + luckyEmployee.dressCode);
    }

    private void fire(Employee luckyEmployee) {
        luckyEmployee.dressCode = "tshirt";
        System.out.println(luckyEmployee.firstname + " has been fired! Dress with : " + luckyEmployee.dressCode);
    }

    public void assignNewSalary(Employee luckyEmployee, int newSalary) {
        // เขียนให้ตรวจว่าเงินเดือนน้อยกว่าเดิมหรือไม่
        // หากเงินเดือนน้อยกว่าเดิมให้ขึ้นข้อความว่า <ชื่อพนักงาน>'s salary is less than
        // before!! และไม่ต้อง set เงินเดือนใหม่ให้พนักงานคนนั้น
        // หากเงินเดือนมากกว่าเดิมให้ set เงินเดือนใหม่ให้สำเร็จ และขึ้นข้อความว่า
        // <ชื่อพนักงาน>'s salary has been set to <newSalary>
        if (newSalary <= luckyEmployee.getSalary()) {
            System.out.println(luckyEmployee.firstname + "'s salary is less than before!!");
        } else {
            System.out.println(luckyEmployee.firstname + "'s salary has been set to " + newSalary);
            super.setSalary(newSalary);
        }
    }

    private void golf() { // simulate private method
        this.dressCode = "golf_dress";
        System.out.println("He goes to golf club to find a new connection. Dress with:" + this.dressCode);
    };

    private Programmer convertDataToProgrammer(String rawData) {

        String[] datas = rawData.split(" ");
        String[] programmerInfo = new String[datas.length];

        for (int i = 0; i < datas.length; i++) {
            String[] info = datas[i].split(":");
            programmerInfo[i] = info[1];
        }

        return new Programmer(Integer.parseInt(programmerInfo[0]), programmerInfo[1], programmerInfo[2],
                Integer.parseInt(programmerInfo[3]),
                programmerInfo[4]);
    }

    public void printEmployees() {
        for (Programmer programmer : employees) {
            System.out.printf("id: %d firstname: %s lastname: %s salary: %d type: %s \n", programmer.id,
                    programmer.firstname, programmer.lastname, programmer.getSalary(), programmer.type);
        }
    }

}
