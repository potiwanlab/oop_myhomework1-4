package my_oop_jar;

public class OfficeCleaner extends Employee {

    public OfficeCleaner(int id, String firstnameInput, String lastnameInput, int salaryInput, String type) {
        super(id, firstnameInput, lastnameInput, salaryInput, type);
    }

    @Override
    public void work() {
        clean();
        killCoachroach();
        decorateRoom();
        welcomeGuest();
    }

    public void clean() {
        System.out.println("Office cleaner clean");
    }

    public void killCoachroach() {
        System.out.println("Office cleaner kill coachroach ");
    }

    public void decorateRoom() {
        System.out.println("Office cleaner decorate room");
    }

    public void welcomeGuest() {
        System.out.println("Office cleaner welcome guest");
    }
}
